import argparse
import os
import requests
import pandas as pd
import xml_tools

#TODO: run from the command line
#TODO: export to .csv instead of excel
#TODO: produce separate file to store manual methodology annotations
#TODO: perhaps don't produce so many files, only the eventual catalog?


if not os.path.exists('output'):
    os.makedirs('output')


# Study (Sequence) metadata

name = 'virome_studies_sequence'

query = 'virome'
domain = 'study'
#
# result = requests.get('http://www.ebi.ac.uk/ena/data/search?query={0}&result={1}&display=xml'.format(query, domain))
#
# study_data = result.text
#
# with open('output/{0}_results.xml'.format(name), 'w', encoding='utf8') as file:
#     file.write(study_data)
#
# seq_df = xml_tools.xml_to_df(study_data)
# seq_df.to_pickle('output/{0}_results.pickle'.format(name))
# seq_df.to_excel('output/{0}_results.xlsx'.format(name))
#
seq_df = pd.read_pickle('output/virome_studies_sequence_results.pickle')

columns_to_use = [
    ('PROJECT', 'IDENTIFIERS', 'PRIMARY_ID', '', '', '<PRIMARY_ID'),
    ('PROJECT', 'IDENTIFIERS', 'SECONDARY_ID', '', '', '<SECONDARY_ID'),
    ('PROJECT', 'SUBMISSION_PROJECT', 'ORGANISM', 'SCIENTIFIC_NAME', '', '<SCIENTIFIC_NAME'),
    ('PROJECT', 'SUBMISSION_PROJECT', 'ORGANISM', 'TAXON_ID', '', '<TAXON_ID')
]

seq_df = seq_df[columns_to_use]
seq_df.columns = ['Primary_ID', 'Secondary_ID', 'Scientific_Name', 'Taxon_ID']
seq_df = seq_df.reset_index(drop=True)
#
# # Study meta data:
#
name = 'virome_studies'
query = 'virome'
domain = 'sra-study'
#
# result = requests.get('https://www.ebi.ac.uk/ena/browser/api/xml/textsearch?domain={0}&query={1}'.format(domain, query))
#
# study_data = result.text
#
# with open('output/{0}_results.xml'.format(name), 'w', encoding='utf8') as file:
#     file.write(study_data)
#
# main_df = xml_tools.xml_to_df(study_data)
# main_df.to_pickle('output/{0}_results.pickle'.format(name))
# main_df.to_excel('output/{0}_results.xlsx'.format(name))

main_df = pd.read_pickle('output/{0}_results.pickle'.format(name))
pubmed_col = main_df[('STUDY', 'STUDY_LINKS', 'STUDY_LINK0', 'XREF_LINK', 'DB', '<DB')]
has_pubmed = pubmed_col.apply(lambda x: x.upper() if isinstance(x, str) else x) == 'PUBMED'

in_pubmed_df = main_df[has_pubmed]

columns_to_use = [
    ('STUDY', 'IDENTIFIERS', 'PRIMARY_ID', '', '', '<PRIMARY_ID'),
    ('STUDY', 'IDENTIFIERS', 'SECONDARY_ID', '', '', '<SECONDARY_ID'),
    ('STUDY', 'DESCRIPTOR', 'STUDY_TITLE', '', '', '<STUDY_TITLE'),
    ('STUDY', 'DESCRIPTOR', 'STUDY_TYPE', '', '', 'existing_study_type'),
    ('STUDY', 'STUDY_LINKS', 'STUDY_LINK0', 'XREF_LINK', 'ID', '<ID')
]


fine_df = in_pubmed_df[columns_to_use]
fine_df.columns = ['Primary_ID', 'Secondary_ID', 'Title', 'Study_Type', 'Pubmed_link']
fine_df = fine_df.reset_index(drop=True)

# df1['Total2'] = df1['Name'].map(df2.set_index('Name')['Total2'])
fine_df['Taxon_ID'] = fine_df['Secondary_ID'].map(seq_df.set_index('Primary_ID')['Taxon_ID'])
fine_df['Scientific_Name'] = fine_df['Secondary_ID'].map(seq_df.set_index('Primary_ID')['Scientific_Name'])

pmid_list = fine_df['Pubmed_link'].values
topics = xml_tools.pubmed_topics(pmid_list)

fine_df['Major_Topics'] = fine_df['Pubmed_link'].map(topics['major'])
fine_df['Minor_Topics'] = fine_df['Pubmed_link'].map(topics['minor'])
fine_df['Keywords'] = fine_df['Pubmed_link'].map(topics['keywords'])

info_items = ['n_runs', 'accessions', 'files', 'n_files', 't_file_size', 't_reads', 'avg_reads']

fastq_info = {item: {} for item in info_items}

study_ids = fine_df['Primary_ID'].values
for sid in study_ids:
    print(sid)
    study_info = xml_tools.fastq_info(sid)
    print(study_info)
    for item in info_items:
        fastq_info[item][sid] = study_info[item]

for item in info_items:
    fine_df[item] = fine_df['Primary_ID'].map(fastq_info[item])

fine_df['t_file_size'] = (fine_df['t_file_size']/10**6).round(0)
fine_df.rename(columns={'t_file_size': 'Total_File_Size (MB)'}, inplace=True)

print(fine_df.columns)
fine_df.to_pickle('output/virome_studies_refined.pickle')


def make_pubmed_excel_link(val):
    return '=HYPERLINK("https://www.ncbi.nlm.nih.gov/pubmed/?term={0}", "{0}")'.format(val)


excel_df = fine_df.assign(Pubmed_link = lambda df: df.Pubmed_link.apply(lambda x: make_pubmed_excel_link(x)))

from StyleFrame import StyleFrame, Styler

excel_writer = StyleFrame.ExcelWriter('output/virome_studies_refined.xlsx')
style = Styler(font_size=11, font='Calibri', horizontal_alignment='left', wrap_text=True)

sf = StyleFrame(excel_df, style)
sf.apply_headers_style(style)
print(sf.columns)
sf.apply_column_style(['n_runs', 'n_files', 'Total_File_Size (MB)', 't_reads', 'avg_reads'],
                      Styler(font_size=11, font='Calibri', number_format='#,##0', horizontal_alignment='right'))
sf.set_column_width(['Major_Topics', 'Minor_Topics', 'Keywords'], 35)
sf.set_row_height([i+1 for i in range(len(sf)+1)], 15)

sf.P_FACTOR = 0.8 # adjust width of column
sf.to_excel(excel_writer=excel_writer, columns_and_rows_to_freeze='A2', best_fit=['Primary_ID', 'Secondary_ID', 'Title', 'Study_Type',
 'Scientific_Name'])
print(excel_df.columns.values)
excel_writer.save()



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('query', action='store', nargs=1, type=str)
    parser.add_argument('-o', '--output_dir', action='store', nargs=1, type=str)
    parser.add_argument('-n', '--max_files', action='store', default=10, type=int)
    parser.add_argument('-t', '--test_run', action='store_true')
    parser.add_argument('--refresh_metadata', action='store_true')

    args = parser.parse_args()

