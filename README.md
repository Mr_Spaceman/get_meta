# get_meta [WIP]

A utility that handles downloading data from `ENA` repositories.


## Requirements

This is not a fully packaged solution (yet) so you will only require python and the packages in `requirements.txt`

## Installation

The quick way
```
$ pip install -f requirements.txt
$ python download_fastq.py -h

usage: download_fastq.py [-h] [-n MAX_FILES] [--refresh_metadata] study_catalog output_dir

Download fastq files from metagenomic studies in the ENA database. Select study IDs from the catalog obtained with find_genomes.py by placing an 'X' in the 'select' column.First, a list is generated from the catalog and saved to 'selected_studies.txt' in the output folder. If this file is already
present, study IDs will be taken from this file instead.Meta data on the run files (fastq) is downloaded for each study andplaced in the study subdirectory. The meta data is checkt to determine if studies are pair-ended or single and studies with inconsistent or mixed files are ignored. Downloading can
be forced by placing the study IDs in 'force_download.txt', separated by a new line in the output folder.The files are downloaded per accession number. A maximum number of files per study can be specified, and new accessions wil be downloaded randomly until this maximum number is passed.If fastq files
are already present in the output folder, they will bechecked against the checksum in the meta data. If the checksums do not match, the local file is deleted and downloaded, else it will be skipped. Accession for which some files are not present will be completed regardless of max file number. If the
max file number is not exceeded, additional runs will be downloaded.

positional arguments:
  study_catalog
  output_dir

optional arguments:
  -h, --help            show this help message and exit
  -n MAX_FILES, --max_files MAX_FILES
                        specify a maximum number of files per study
  --refresh_metadata    force downloading of run meta data

```

The recommended way is to create a virtual environment with your favorite environment manager (e.g conda), so all
dependencies are nicely isolated in there

```
$ conda create my_env pip
$ conda activate my_env
(my_env)$ pip install -f requirements.txt 
(my_env)$ python download_fastq.py -h

```

## Usage (TBD)

Primarily we want to focus on getting `fastq` files from the [ENA](https://www.ebi.ac.uk/ena) repositories, hence
 `download_fastq.py`.

Queries can be posted as well with `find_genomes.py`, mainly viromes as the search term for now.