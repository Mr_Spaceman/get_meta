import requests
import pandas as pd
import numpy as np
import xml.etree.ElementTree as ET

def deconstruct(e):
    txt = e.text if e.text else ''

    if len(e) == 0:
        c = ['<' + e.tag] + [k for k in e.keys()]
        return [[e.tag]*len(c), c], [txt.strip()]+[e.get(k).strip() for k in e.keys()]
    else:
        columns = [[],[]]
        v = []
        for i, f in enumerate(e):
            cv = deconstruct(f)

            c = cv[0]

            tags = [child.tag for child in e]

            c[0] = [tag+str(i) if tags.count(tag) > 1 else tag for tag in c[0]]

            columns_bottom = columns.pop(-1)
            c_bottom = c.pop(-1)
            l_new = len(c)
            l_old = len(columns)
            if l_new > l_old:
                for j, level in enumerate(columns):
                    columns[j] = level + c.pop(0)
                extra_cols = ['']*(len(columns[j])-len(c[0]))

                for level in c:
                    columns.append(extra_cols+level)

                columns.append(columns_bottom+c_bottom)

            if l_new <= l_old:
                for j, level in enumerate(columns[:l_new]):
                    columns[j] = level + c.pop(0)

                extra_cols = [''] * (len(columns[j]) - len(columns_bottom))

                for level in columns[l_new:]:
                    level += extra_cols

                columns.append(columns_bottom + c_bottom)

            v += [value.strip() for value in cv[1]]

        new_cols = ['<'+e.tag]+[k for k in e.keys()]

        extra_cols = ['']*len(new_cols)

        for j, level in enumerate(columns[:-1]):
            columns[j] = extra_cols + level

        columns[-1] = new_cols+columns[-1]

        columns.insert(0, [e.tag]*len(columns[-1]))

        return columns, [txt]+[e.get(k) for k in e.keys()]+v
        # return [[e.tag]*len(c)]+[c], [txt]+[e.get(k) for k in e.keys()]+v

def xml_to_df(study_data):
    studies = ET.fromstring(study_data)

    main_df = pd.DataFrame()

    for study in studies:
        main_df_ncol = main_df.columns
        main_df_nlevels = main_df_ncol.nlevels

        columns, values = deconstruct(study)

        nlevels = len(columns)
        ncols = len(columns[0])

        if nlevels > main_df_nlevels:
            new_index = np.array(main_df.columns).tolist()
            if len(new_index) > 0:
                for i in range(nlevels - main_df_nlevels):
                    new_index.insert(-1, [''] * len(new_index[0]))
                main_df = pd.DataFrame(main_df.values, columns=pd.MultiIndex.from_arrays(new_index))

        if nlevels < main_df_nlevels:
            for i in range(main_df_nlevels - nlevels):
                columns.insert(-1, [''] * len(columns[0]))

        df = pd.DataFrame(np.array([values]), columns=columns)
        main_df = main_df.append(df)

    # delete empty data:
    main_df = main_df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    main_df.replace('', np.nan, inplace=True)
    main_df.dropna(how='all', axis=1, inplace=True)

    return main_df


def pubmed_topics(pmid_list):
    pmids = ','.join(pmid_list)
    request = requests.get(
        'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id={0}&retmode=xml'.format(pmids))
    # print(request.text)
    articleset = ET.fromstring(request.text)
    articles = articleset.findall('PubmedArticle')

    major = {}
    minor = {}
    keyword_lists = {}
    for article in articles:
        pmid = article.find('MedlineCitation').find('PMID').text


        article_keywords = []

        for keyword_list in article.iter('KeywordList'):
            for keyword in keyword_list.findall('Keyword'):
                article_keywords.append(keyword.text)

        major_topics = []
        minor_topics = []

        for mesh_list in article.iter('MeshHeadingList'):
            for heading in mesh_list.iter('MeshHeading'):
                descriptor = heading.find('DescriptorName')

                qualifiers = heading.findall('QualifierName')

                # print(descriptor.text, descriptor.get('MajorTopicYN'))
                if len(qualifiers) > 0:
                    for qualifier in qualifiers:
                        # print(descriptor.text, qualifier.text)
                        qualifier_name = descriptor.text + ' ' + qualifier.text
                        if qualifier.get('MajorTopicYN') == 'Y' or descriptor.get('MajorTopicYN') == 'Y':
                            major_topics.append(qualifier_name)
                            # print(qualifier_name, descriptor.get('MajorTopicYN'))
                        else:
                            minor_topics.append(qualifier_name)
                else:
                    if descriptor.get('MajorTopicYN') == 'Y':
                        major_topics.append(descriptor.text)
                    else:
                        minor_topics.append(descriptor.text)
        # print(pmid, 'keyw: ', len(article.find('MedlineCitation').findall('KeywordList')), 'mesh: ', len(major_topics))
        major[pmid] = '; '.join(major_topics)
        minor[pmid] = '; '.join(minor_topics)
        keyword_lists[pmid] = '; '.join(article_keywords)

    return {'major': major, 'minor': minor, 'keywords': keyword_lists}


def fastq_info(study_id):
    request = requests.get(
        'https://www.ebi.ac.uk/ena/data/warehouse/filereport?accession={0}&result=read_run&fields=run_accession,fastq_ftp,fastq_md5,fastq_bytes,read_count,base_count,library_layout'.format(study_id))

    fastq = request.text.strip()

    rows = fastq.split('\n')
    columns = [row.split('\t') for row in rows]
    header = columns.pop(0)
    if len(columns) > 0:

        data = {}
        for i, column in enumerate(header):
            data[column] = []
            for row in columns:
                data[column].append(row[i])

        # print(data)
        n_runs = len(data['run_accession'])
        files = ';'.join(data['fastq_ftp']).split(';')
        accessions = {accession: data['fastq_ftp'][i].split(';') for i, accession in enumerate(data['run_accession'])}
        n_files = len(files)
        file_size = [int(s) for s in ';'.join(data['fastq_bytes']).split(';')]
        t_file_size = sum(file_size)
        reads = [int(s) for s in ';'.join(data['read_count']).split(';')]
        t_reads = sum(reads)
        avg_reads = int(sum(reads) / len(reads))
    else:
        n_runs = None
        files = None
        accessions = None
        n_files = None
        t_file_size = None
        t_reads = None
        avg_reads = None

    return {'n_runs': n_runs, 'accessions': accessions, 'files': files, 'n_files': n_files, 't_file_size': t_file_size, 't_reads': t_reads, 'avg_reads': avg_reads}


