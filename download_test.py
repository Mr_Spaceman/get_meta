import os
import hashlib
import pandas as pd
import download_fastq as dl

def get_meta():
    accessions= {
            'run_accession': [],
            'fastq_ftp': [],
            'fastq_md5': [],
            'library_layout': []
    }

    return accessions

def gen_md5(t):
    return hashlib.md5(str.encode(t)).hexdigest()


def generate_accessions(accessions, s, n, layout, n_files):
    for i in range(s, n+1):
        accession_name = 'test_{0:03d}_{1}_{2}'.format(i, layout, n_files)
        accessions['run_accession'].append(accession_name)

        file_types = {
            'single': accession_name + '.fastq.gz',
            'paired': accession_name +'_1.fastq.gz;' +
                      accession_name +'_2.fastq.gz',
            'multi_paired': accession_name + '.fastq.gz;' +
                            accession_name + '_1.fastq.gz;' +
                            accession_name + '_2.fastq.gz',
            'multi': accession_name + '_1.fastq.gz;' +
                     accession_name + '_2.fastq.gz;' +
                     accession_name + '_3.fastq.gz;' +
                     accession_name + '_4.fastq.gz'
        }

        md5_types = {
            'single': gen_md5(accession_name),
            'paired': gen_md5(accession_name)+ ';' +
                      gen_md5(accession_name),
            'multi_paired': gen_md5(accession_name)+ ';' +
                            gen_md5(accession_name)+ ';' +
                            gen_md5(accession_name),
            'multi': gen_md5(accession_name) + ';' +
                     gen_md5(accession_name)+ ';' +
                     gen_md5(accession_name)+ ';' +
                     gen_md5(accession_name)
        }
        accessions['fastq_ftp'].append(file_types[n_files])
        accessions['fastq_md5'].append(md5_types[n_files])
        accessions['library_layout'].append(layout)

    return accessions

studies = {
    '1single_study': generate_accessions(get_meta(), 1, 10, 'SINGLE', 'single'),
    '2paired_study': generate_accessions(get_meta(), 1, 10, 'PAIRED', 'paired'),
    '3single_multi_paired': generate_accessions(get_meta(), 1, 10, 'SINGLE', 'multi_paired'),
    '4single_multi': generate_accessions(get_meta(), 1, 10, 'SINGLE', 'multi'),
    '5paired_multi_paired': generate_accessions(get_meta(), 1, 10, 'PAIRED', 'multi_paired'),
    '6paired_multi': generate_accessions(get_meta(), 1, 10, 'PAIRED', 'multi'),
    '7combi_matching': generate_accessions(
                       generate_accessions(get_meta(), 1, 10, 'SINGLE', 'single'),
                                                      11, 20, 'PAIRED', 'paired')
}


# print(studies)

for study in studies.keys():
    study_path = os.path.join('remote', study)
    if not os.path.isdir(study_path):
        os.makedirs(study_path)
    for i, accession in enumerate(studies[study]['run_accession']):
        files = studies[study]['fastq_ftp'][i].split(';')
        # print(files)
        for file in files:
            if len(file)>0:
                # print(file)
                file_path = os.path.join('remote', study, file)
                with open(file_path, 'w') as writer:
                    writer.write(accession)

for study_id in studies.keys():
    # print(pd.DataFrame.from_dict(studies[study_id]))
    dl.select_runs(study_id, pd.DataFrame.from_dict(studies[study_id]), os.path.join('.', 'data'), 2)
