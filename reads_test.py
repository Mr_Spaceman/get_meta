import requests
import xml_tools
import xml.etree.ElementTree as ET
import pandas as pd
import json

seq_df = pd.read_pickle('output/virome_studies_sequence_results.pickle')
tree = ET.parse('output/virome_studies_sequence_results.xml')

project_id = 'ERP107926'
# PRJEB21210 PRJEB28510

# for project in tree.iter('PROJECT'):
#     runs = []
#     fastq = []
#     if project.find('IDENTIFIERS').find('PRIMARY_ID').text == project_id or True:
#         for link in project.iter('XREF_LINK'):
#             if link.find('DB') is not None:
#                 if link.find('DB').text == 'ENA-RUN':
#                     runs.append(link.find('ID').text)
#             if link.find('DB') is not None:
#                 if link.find('DB').text == 'ENA-FASTQ-FILES':
#                     fastq.append(link.find('ID').text)
#
#     print(fastq)

request = requests.get('https://www.ebi.ac.uk/ena/data/warehouse/filereport?accession={0}&result=read_run&fields=run_accession,fastq_ftp,fastq_md5,fastq_bytes,read_count,base_count'.format(project_id))
fastq = request.text.strip()

rows = fastq.split('\n')
columns = [row.split('\t') for row in rows]
header = columns.pop(0)

print(request.text)
print(columns)
print(header)

data = {}
for i, column in enumerate(header):
    data[column] = []
    for row in columns:
        data[column].append(row[i])

print(data)

n_runs = len(data['run_accession'])
n_files = len(';'.join(data['fastq_ftp']).split(';'))
file_size = [int(s) for s in ';'.join(data['fastq_bytes']).split(';')]
t_file_size = sum(file_size)/10**6
reads = [int(s) for s in ';'.join(data['read_count']).split(';')]
t_reads = sum(reads)
avg_reads = sum(reads)/len(reads)

print(n_runs)
print(n_files)
print(t_file_size)
print(t_reads)
print(int(avg_reads))

# pri
# https://www.ebi.ac.uk/ena/data/warehouse/filereport?accession=PRJEB21210&result=read_run&fields=run_accession,fastq_ftp,fastq_md5,fastq_bytes,read_count,base_count
# https://www.ebi.ac.uk/ena/portal/api/filereport?accession=PRJEB28510&result=read_run&fields=study_accession,sample_accession,experiment_accession,run_accession,tax_id,scientific_name,fastq_ftp,submitted_ftp,sra_ftp&format=json&download=true

