import argparse

parser = argparse.ArgumentParser(description="download_fastq.py [study_ids] [output_dir]"
                                             "Download the fastq files associated with studies in the ENA database."
                                             "From a list of [study_ids], randomly select a subset (or all) files and"
                                             "download these to out_dir.")
parser.add_argument('study_ids', action='store', nargs=1, type=str)
parser.add_argument('output_dir', action='store', nargs=1, type=str)
parser.add_argument('-n', '--max_files', action='store', default=10, type=int)

args = parser.parse_args()

print(args.study_ids, args.output_dir, args.max_files)
